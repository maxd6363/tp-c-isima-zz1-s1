#include "input.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include "block.h"
#include "game.h"
#include "color.h"
#include "setting.h"

Block input_getUserColor(int numberColors){
	Block key;
	printf("Choose color : ");
	for(int i=0;i<numberColors;i++){
		input_setColorText(i);
		printf("%d ",i);
	}
	input_setColorText(-1);
	printf("\n");
	input_hideCursor();
	key = (Block)(keyDetec() - 48);
	input_showCursor();
	return key;

}

Block input_getUserColorSmart(Grid grid, int round, Settings setting){
	int key = -1;
	do{
		if(key != -1){
			game_printGame(grid, round, setting.maxRound);
			printf("Choose a valid color\n");
		}
		key = input_getUserColor(setting.numberColors);
	}while(key < 0 || key >= setting.numberColors);
	return (Block)key;

}



int keyDetec(void){
	int c;   
	static struct termios oldt, newt;
	tcgetattr( STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON);          
	tcsetattr( STDIN_FILENO, TCSANOW, &newt);
	c=getchar();
	tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
	return c;
}


void input_clear(void){
	system("clear");
}

void input_hideCursor(){
	system("stty -echo");
}



void input_showCursor(){
	system("stty echo");
}


void input_setColorText(int index){
	switch(index){
		case -1:
		printf(RESET);
		break;
		case 0:
		printf(RED);
		break;
		case 1:
		printf(GRE);
		break;
		case 2:
		printf(YEL);
		break;
		case 3:
		printf(BLU);
		break;
		case 4:
		printf(MAG);
		break;
		case 5:
		printf(CYA);
		break;
	}
}













