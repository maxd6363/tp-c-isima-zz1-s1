#ifndef SETTING_H
#define SETTING_H

typedef struct{
	int maxRound;
	int gridSize;
	int numberColors;

}Settings;

Settings setting_load(const char* filename);



#endif