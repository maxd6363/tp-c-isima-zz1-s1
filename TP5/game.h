#ifndef GAME
#define GAME


#include "grid.h"
#include "setting.h"


void game_entry(void);

void game_start(Settings settings);

void game_printGame(Grid grid, int round, int maxRound);

void game_end(Grid grid, bool won);

#endif