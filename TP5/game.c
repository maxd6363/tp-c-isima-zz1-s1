#include "game.h"

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "input.h"
#include "grid.h"
#include "block.h"
#include "setting.h"
#include "bool.h"






void game_entry(void){
	input_clear();
	printf("\n\n\n\n\n\n\n\n\n\n");
	printf("\t__/\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\__/\\\\\\\\\\\\________________________________________/\\\\\\___/\\\\\\\\\\\\\\\\\\\\\\_______________        \n");
	printf("\t _\\/\\\\\\///////////__\\////\\\\\\_______________________________________\\/\\\\\\__\\/////\\\\\\///________________       \n");
	printf("\t  _\\/\\\\\\________________\\/\\\\\\_______________________________________\\/\\\\\\______\\/\\\\\\_________/\\\\\\______      \n");
	printf("\t   _\\/\\\\\\\\\\\\\\\\\\\\\\________\\/\\\\\\________/\\\\\\\\\\________/\\\\\\\\\\___________\\/\\\\\\______\\/\\\\\\______/\\\\\\\\\\\\\\\\\\\\\\_     \n");
	printf("\t    _\\/\\\\\\///////_________\\/\\\\\\______/\\\\\\///\\\\\\____/\\\\\\///\\\\\\____/\\\\\\\\\\\\\\\\\\______\\/\\\\\\_____\\////\\\\\\////__    \n");
	printf("\t     _\\/\\\\\\________________\\/\\\\\\_____/\\\\\\__\\//\\\\\\__/\\\\\\__\\//\\\\\\__/\\\\\\////\\\\\\______\\/\\\\\\________\\/\\\\\\______   \n");
	printf("\t      _\\/\\\\\\________________\\/\\\\\\____\\//\\\\\\__/\\\\\\__\\//\\\\\\__/\\\\\\__\\/\\\\\\__\\/\\\\\\______\\/\\\\\\________\\/\\\\\\_/\\\\__  \n");
	printf("\t       _\\/\\\\\\______________/\\\\\\\\\\\\\\\\\\__\\///\\\\\\\\\\/____\\///\\\\\\\\\\/___\\//\\\\\\\\\\\\\\/\\\\__/\\\\\\\\\\\\\\\\\\\\\\____\\//\\\\\\\\\\___ \n");
	printf("\t        _\\///______________\\/////////_____\\/////________\\/////______\\///////\\//__\\///////////______\\/////____\n");
	usleep(4000000);
}



void game_start(Settings settings){
	Grid g;
	Block choice;
	bool playerWon = false;
	srand(time(NULL));
	int round = 0;
	g = grid_init(settings.gridSize);
	grid_rand(g, settings.numberColors);

	while(!playerWon && round <= settings.maxRound){
		game_printGame(g, round, settings.maxRound);
		choice = input_getUserColorSmart(g, round, settings);
		grid_spreadColor(g, choice);
		if(grid_isUniform(g)){
			playerWon = true;
		}
		round++;
	}
	game_end(g, playerWon);
	grid_free(&g);
}


void game_end(Grid grid ,bool won){
	if(won){
		printf("You won the game !\n");
	}
	else{
		printf("You lose the game !\nBut you have filled %.2lf%% of the grid :)\n", grid_fillPercentage(grid)* 100);
	}
}


void game_printGame(Grid grid, int round, int maxRound){
	input_clear();
	printf("Round : %2d / %2d\n",round, maxRound);
	grid_print(grid);
}