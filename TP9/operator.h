#ifndef OPERATOR
#define OPERATOR


typedef enum {
  NONE = -1,
  ID,
  SIN,
  COS,
  LOG,
  EXP

} Operator;


Operator operator_identify(const char * str);
double operator_eval(Operator op, double value);
const char * operator_to_string(Operator op);



#endif //OPERATOR