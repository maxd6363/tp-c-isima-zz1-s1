#include "operator.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#include "string.h"



Operator operator_identify(const char * str){
	if(str == NULL){
		return NONE;
	}


	if(string_equals(str, "x")){
		return ID;
	}
	if(string_equals(str, "sin(x)")){
		return SIN;
	}
	if(string_equals(str, "cos(x)")){
		return COS;
	}
	if(string_equals(str, "log(x)")){
		return LOG;
	}
	if(string_equals(str, "exp(x)")){
		return EXP;
	}

	return NONE;
}



double operator_eval(Operator op, double value){
	switch(op){
		case ID:
		return value;

		case SIN:
		return sin(value);

		case COS:
		return cos(value);

		case LOG:
		return log(value);

		case EXP:
		return exp(value);

		default :
		fprintf(stderr, "Error with operator NONE ! \n");
		return 0.0;
	}

}


const char * operator_to_string(Operator op){
		switch(op){
		case ID:
		return "x";
		case SIN:
		return "SIN(x)";
		case COS:
		return "COS(x)";
		case LOG:
		return "LOG(x)";
		case EXP:
		return "EXP(x)";
		default :
		return "ERROR";
	}

}