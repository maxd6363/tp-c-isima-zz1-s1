#include "string.h"

#include <string.h>


bool string_contain(const char * str, const char* exp){
	if(str == NULL || exp == NULL){
		return false;
	}
	return strstr(str, exp) == NULL ? false : true;
}


bool string_equals(const char * str, const char* exp){
	if(str == NULL || exp == NULL){
		return false;
	}
	return strcmp(str, exp) != 0 ? false : true;
}