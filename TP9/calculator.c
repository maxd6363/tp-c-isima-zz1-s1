#include "calculator.h"

#include <stdio.h>
#include <stdlib.h>



bool calculator_process(Operator op, double intervalBegin, double intervalEnd, double intervalDelta, const char *filename ){
	if(op == NONE || filename == NULL || intervalBegin > intervalEnd || intervalDelta <= 0){
		return false;
	}

	FILE * file = fopen(filename,"w");
	double value;

	if(file==NULL){
		fprintf(stderr, "Can't open file %s ...\n",filename );
		return false;
	}

	for(double i=intervalBegin;i<=intervalEnd;i+=intervalDelta){
		value=operator_eval(op, i);
		fprintf(file, "%lf : %lf\n",i, value);
	}
	fclose(file);
	return true;
}
