#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>



#include "string.h"
#include "operator.h"
#include "bool.h"
#include "calculator.h"


int user_input(char * command){
	double intervalBegin;
	double intervalEnd;
	double intervalDelta;
	Operator op = NONE;
	char operatorStr[256];
	char filename[256] = "UNKNOWN";
	
	sscanf(command, "%lf %lf %lf %s %s", &intervalBegin, &intervalEnd, &intervalDelta, operatorStr, filename);
	op = operator_identify(operatorStr);
	if(op == NONE){
		op = ID;
	}
	if(string_equals(filename, "UNKNOWN")){
		sprintf(filename,"%s [%lf ; %lf : %lf].dat",operator_to_string(op), intervalBegin, intervalEnd, intervalDelta);
	}

	printf("[%lf ; %lf : %lf] <%s> <%s>\n", intervalBegin, intervalEnd, intervalDelta, operator_to_string(op), filename);
	
	return calculator_process(op, intervalBegin, intervalEnd, intervalDelta, filename);



}





void help(void){
	printf("compute [begin] [end] [delta] <x | sin(x) | cos(x) | exp(x) | log(x)> <filename>\n");
}


int main(int argc, char const *argv[]){
	if(argc < 4){
		help();
		return 1;
	}
	char command[1024];

	if(argc == 4){
		sprintf(command,"%lf %lf %lf", atof(argv[1]), atof(argv[2]), atof(argv[3]));
	}

	if(argc == 5){
		sprintf(command,"%lf %lf %lf %s", atof(argv[1]), atof(argv[2]), atof(argv[3]), argv[4]);
	}

	if(argc == 6){
		sprintf(command,"%lf %lf %lf %s %s", atof(argv[1]), atof(argv[2]), atof(argv[3]), argv[4], argv[5]);
	}
	

	printf("COMMAND : %s\n",command );
	int res =  user_input(command);
	printf("%d\n", !res);
	return !res;

}
