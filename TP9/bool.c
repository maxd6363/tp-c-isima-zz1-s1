#include "bool.h"

const char * to_string(bool b){
	return b ? "true" : "false";
}