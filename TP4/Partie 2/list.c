#include "list.h"
#include <string.h>


List list_new(void){
	return NULL;
}



void list_free(List *list){
	if(list == NULL || *list == NULL){
		return;
	}
	Maillon *tmp = *list;
	Maillon *toFree = tmp;

	while(tmp != NULL){
		toFree=tmp;
		tmp = tmp->next;
		free(toFree);
	}
	*list = NULL;
}



void list_foreach(List list, void (*pfunc)(donnee_t)){

	if(list == NULL){
		return;
	}
	Maillon *tmp = list;

	while(tmp != NULL){
		pfunc(tmp->value);
		tmp = tmp->next;
	}
}


bool list_add(List *list, donnee_t value){
	Maillon *tmp = (Maillon*)malloc(sizeof(Maillon));
	if(tmp == NULL){
		return false;
	}
	tmp->value.score = value.score;
	strcpy(tmp->value.nom,value.nom);
	strcpy(tmp->value.alias,value.alias);
	tmp->next = *list;
	*list=tmp;
	return true;
}

bool list_delete(List *list, int index){
	if(list == NULL || *list == NULL || index < 0){
		return false;
	}
	Maillon *tmp = *list;
	int i = 0;
	
	if (index==0){
		*list=tmp->next;
		free(tmp);
		return true; 
	}

	while(tmp != NULL && i < index -1){
		tmp=tmp->next;
		i++;
	}

	if(tmp == NULL || tmp->next == NULL){
		return false;
	}

    Maillon *next = tmp->next->next; 
	free(tmp->next);
	tmp->next=next;

	return true;
}



void list_modify(List *list, int index, donnee_t donnee){
	if(list == NULL || *list == NULL || index < 0){
		return false;
	}
	Maillon *tmp = *list;
	int i = 0;

	while(tmp != NULL && i < index -1){
		tmp=tmp->next;
		i++;
	}

	if(tmp == NULL || tmp->next == NULL){
		return false;
	}
	tmp->value=donnee;
}