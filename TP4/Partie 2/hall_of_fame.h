#ifndef hall_of_fame_h
#define hall_of_fame_h

#include <stdlib.h>
#include <stdio.h>

/* DECLARATION DES TYPES PERSONNELS */
// et de leur typedef si besoin
typedef struct donnee{
	int score;
	char nom[100];
	char alias[40];
}donnee_t;




/* DECLARATIONS DES METHODES */
void afficherDonnee(FILE *, donnee_t);
void displayDonnee(donnee_t);

void saisirDonnee (FILE * , donnee_t *);
char * getStr(char *str, int size, FILE * flot);
donnee_t* tableauFromFilename(char *file, int *size);




#endif