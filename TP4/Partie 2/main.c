#include "hall_of_fame.h"
#include "bool.h"
#include "list.h"

int main(void){
	donnee_t donne1 = {.score=1, .nom="Minecraft", .alias="Max"};
	donnee_t donne2 = {.score=2, .nom="Minecraft", .alias="Max"};
	donnee_t donne3 = {.score=3, .nom="Minecraft", .alias="Max"};
	donnee_t donne4 = {.score=300, .nom="GTA V", .alias="Max"};

	List list = list_new();
	list_add(&list, donne1);
	list_add(&list, donne2);
	list_add(&list, donne3);
	list_foreach(list, &displayDonnee);
	printf("\n");

	list_modify(&list,0, donne4);
	list_foreach(list, &displayDonnee);

	list_delete(&list,0);
	printf("\n");
	list_foreach(list, &displayDonnee);

	list_free(&list);


	return 0;
}