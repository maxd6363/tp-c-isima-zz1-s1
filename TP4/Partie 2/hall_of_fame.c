#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hall_of_fame.h"

// un petit commentaire ?
//Non
void afficherDonnee(FILE * file, donnee_t d) {
	fprintf(file, "%s : %s avec %d\n",d.nom, d.alias, d.score);
}

char * getStr(char *str, int size, FILE * flot){
	if(str != NULL && flot != NULL){
		fgets(str,size, flot);
		str[strlen(str) - 1] = '\0';
	}
	return str;
}


// un petit commentaire ?
void saisirDonnee(FILE *file, donnee_t * p){
	char score[50];
	getStr(p->nom,100, file);
	getStr(p->alias,40, file);

	fgets(score,50, file);
	p->score = atoi(score);

}


donnee_t* tableauFromFilename(char *file, int *size){
	FILE *flot = fopen(file,"r");
	donnee_t *tab = NULL;
	int arraySize = 0;
	int arrayStep = 10;
	if(flot == NULL){
		return 0;
	}
	if(flot == NULL){
		return 0;
	}

	while(!feof(flot)){
		if(*size>=arraySize){
			arraySize+=arrayStep;
			tab=(donnee_t*)realloc(tab,arraySize);
		}
		saisirDonnee(flot, &tab[*size]);
		afficherDonnee(stdout,tab[*size]);
		(*size)++;
	}
	fclose(flot);
	return tab;
}

void displayDonnee(donnee_t value){
	afficherDonnee(stdout, value);
}