#ifndef LIST_H
#define LIST_H

#include "hall_of_fame.h"
#include "bool.h"


typedef struct maillon{
	donnee_t value;
	struct maillon *next;
}Maillon,*List;

List list_new(void);
void list_free(List *list);
void list_foreach(List list, void (*pfunc)(donnee_t));
bool list_add(List *list, donnee_t value);
bool list_delete(List *list, int index);
void list_modify(List *list, int index, donnee_t donnee);

#endif