#ifndef CELL_H
#define CELL_H


typedef struct cell{

	char *value;
	struct cell *next;

}Cell;


Cell* cell_new(int stringSize, char *value);
void cell_free(Cell *cell);
void cell_disp(Cell cell);



#endif //CELL_H