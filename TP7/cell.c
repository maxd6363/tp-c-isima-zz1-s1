#include "cell.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRING_TMP_SIZE 256


Cell* cell_new(int stringSize, char *value){
	if(stringSize <= 0){
		stringSize = STRING_TMP_SIZE;
	}
	Cell* cell=(Cell*)malloc(sizeof(Cell));
	if(cell != NULL){
		cell->next=NULL;
		cell->value=(char*)calloc(stringSize,sizeof(char));
		if(cell->value == NULL){
			free(cell);
			cell=NULL;
		}
		else{
			if(value != NULL){
				strcpy(cell->value, value);
			}
		}
	}
	return cell;
}







void cell_free(Cell *cell){
	if(cell != NULL){
		if(cell->value != NULL){
			free(cell->value);
		}
		free(cell);
	}
}

void cell_disp(Cell cell){
	printf("%s\n",cell.value);
}