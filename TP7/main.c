#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "list.h"
#include "string.h"


#define TMP_SIZE 1024

void test1(void){
	List list = list_new();

	list_puch_front(&list, "FRONT TEST 1");
	list_puch_front(&list, "FRONT TEST 2");
	list_puch_front(&list, "FRONT TEST 3");

	printf("SEG\n");
	list_puch_back(&list, "BACK TEST 1");
	list_puch_back(&list, "BACK TEST 2");
	list_puch_back(&list, "BACK TEST 3");

	
	list_foreach(list, cell_disp);
	list_free(&list);

}


void fill_list(const char *fileName){
	FILE * file;
	char tmp[TMP_SIZE];
	List list = list_new();


	if(fileName == NULL){
		file = stdin;
	}
	else{
		file=fopen(fileName,"r");
	}

	fgets(tmp,TMP_SIZE, file);
	tmp[strlen(tmp)-1] = '\0';
	while(!feof(file)){
		list_puch_back(&list, tmp);
		fgets(tmp,TMP_SIZE, file);
		tmp[strlen(tmp)-1] = '\0';
	}
	printf("\n----------------------\n\n");
	list_foreach(list, cell_disp);
	list_free(&list);
	if(fileName != NULL){
		fclose(file);	
	}


}

void process_c_file(const char *fileName){
	FILE * file;
	char tmp[TMP_SIZE];
	List list = list_new();
	int parenthesisOpened = 0;

	file=fopen(fileName,"r");
	

	fgets(tmp,TMP_SIZE, file);
	tmp[strlen(tmp)-1] = '\0';
	while(!feof(file)){
		if(contains(tmp,'#')){
			list_puch_back(&list, tmp);
		}
		if(parenthesisOpened == 0){
			if(contains(tmp,'{')){
				parenthesisOpened++;
				replace(tmp,'{',';');
				list_puch_back(&list, tmp);
			}
		}
		else{
			if(contains(tmp,'}')){
				parenthesisOpened--;
			}	
			if(contains(tmp,'{')){
				parenthesisOpened++;
			}	
		}

		//printf("%d %s\n",parenthesisOpened, tmp );

		fgets(tmp,TMP_SIZE, file);
		tmp[strlen(tmp)-1] = '\0';
	}
	printf("\n----------------------\n\n");
	list_foreach(list, cell_disp);
	list_free(&list);
	fclose(file);	

}



int main(int argc, char const *argv[]){
	
	const char *fileName = NULL;
	if(argc != 1){
		fileName=argv[1];
		process_c_file(fileName);
	}
	//fill_list(fileName);

	return 0;
}