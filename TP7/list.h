#ifndef LIST_H
#define LIST_H

#include <stdio.h>
#include "bool.h"
#include "cell.h"





typedef struct {
	Cell *front;
	Cell *back;
}List;


List list_new(void);
void list_free(List *list);

bool list_puch_front(List *list, char *value);
bool list_puch_back(List *list, char *value);

void list_foreach(List list, void (*pfunc)(Cell));


#endif //LIST_H