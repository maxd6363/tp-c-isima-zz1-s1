#include "list.h"
#include "cell.h"

#include <string.h>

List list_new(void){
	List list;
	list.front = NULL;
	list.back = NULL;
	return list;
}


void list_free(List *list){
	Cell * tmp = list->front;
	Cell *toFree;
	while(tmp != NULL){
		toFree = tmp;
		tmp = tmp->next;
		cell_free(toFree);
	}
	list->front = NULL;
	list->back = NULL;
}



bool list_puch_front(List *list, char *value){
	if(list == NULL || value == NULL){
		return false;
	}
	Cell *cell = cell_new(strlen(value), value);
	if(cell == NULL){
		return false;
	}

	if(list->front == NULL){
		list->back = cell;
	}
	else{
		cell->next = list->front;
	}

	list->front = cell;
	return true;
}


bool list_puch_back(List *list, char *value){
	if(list == NULL || value == NULL){
		return false;
	}
	Cell *cell = cell_new(strlen(value), value);
	if(cell == NULL){
		return false;
	}
	if(list->back == NULL){
		list->front=cell;
	}
	else{
		list->back->next=cell;
	}
	list->back=cell;
	return true;

}

void list_foreach(List list, void (*pfunc)(Cell)){
	Cell * tmp = list.front;
	while(tmp != NULL){
		pfunc(*tmp);
		tmp = tmp->next;
	}
}


