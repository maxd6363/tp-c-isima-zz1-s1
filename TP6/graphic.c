#include "graphic.h"


#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "error.h"
#include "color.h"

int graphic_init(void){
	int errorCode = SDL_Init(SDL_INIT_VIDEO); 
	if (errorCode == -1){
		error(__FILE__,__LINE__, "SDL init error");
	}
	return errorCode;
}




void graphic_exit(void){

	SDL_Quit();

}


SDL_Window* graphic_newWindow(const char* title, int width, int height){
	SDL_Window * window;

	window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_RESIZABLE); 

	if (window == NULL){
		error(__FILE__,__LINE__, "SDL window error"); 
	}
	return window;

}



void graphic_freeWindow(SDL_Window *window){
	SDL_DestroyWindow(window);
	window=NULL;
}



SDL_Renderer* graphic_newRenderer(SDL_Window* window){

	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED );
	if (renderer == NULL){
		error(__FILE__,__LINE__, "SDL window error"); 
	}
	return renderer;
}

void graphic_freeRenderer(SDL_Renderer *renderer){
	SDL_DestroyRenderer(renderer);
	renderer = NULL;
}


void graphic_clearBackground(SDL_Renderer *renderer){
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);
}


void graphic_drawBlock(SDL_Renderer *renderer,int widthBlock, int x, int y, Block block){
	SDL_Rect rect;
	Color c = block_getColor(block);
	SDL_SetRenderDrawColor(renderer,c.r , c.g, c.b, 0);
	rect.x = x * widthBlock;
	rect.y = y * widthBlock;
	rect.w = rect.h = widthBlock;;
	SDL_RenderFillRect(renderer, &rect );
}
