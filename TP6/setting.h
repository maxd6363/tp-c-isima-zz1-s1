#ifndef SETTING_H
#define SETTING_H

typedef struct{
	int maxRound;
	int gridSize;
	int numberColors;
	int windowWidth;
	int windowHeight;
}Settings;

Settings setting_load(const char* filename);
Settings setting_get_default(void);


#endif