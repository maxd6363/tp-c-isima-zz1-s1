#include "setting.h"

#include <stdlib.h>
#include <stdio.h>

Settings setting_load(const char* filename){
	FILE* file;
	Settings settings;
	file = fopen(filename, "r");
	fscanf(file, "%d\n%d\n%d\n%d\n%d\n", &settings.maxRound, &settings.gridSize, &settings.numberColors, &settings.windowWidth,&settings.windowHeight);
	if(settings.gridSize < 2){
		settings.gridSize = 2;
	}
	if(settings.maxRound < 1){
		settings.maxRound = 1;
	}
	if(settings.numberColors < 2){
		settings.numberColors = 2;
	}
	if(settings.numberColors > 7){
		settings.numberColors = 7;
	}



	fclose(file);
	return settings;
}

Settings setting_get_default(void){
	Settings settings = {.maxRound=23,.gridSize=12,.numberColors=6, .windowWidth=1280, .windowHeight=720};
	return settings;
}