#ifndef BLOCK_H
#define BLOCK_H

#include <SDL2/SDL.h>

#include "color.h"



typedef enum{B_RED, B_GREEN, B_YELLOW, B_BLUE, B_MAGENTA, B_CYAN, B_GREY} Block;

void block_print(Block b);

Color block_getColor(Block b);

int block_getWidth(SDL_Window *window, int gridSize);

#endif