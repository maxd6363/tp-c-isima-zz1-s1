#include "tools.h"

#include <time.h>
#include <math.h>

long tools_getCurrentMS(void){
	struct timespec _t;
	clock_gettime(CLOCK_REALTIME, &_t);
	return _t.tv_sec*1000 + lround(_t.tv_nsec/1.0e6);
}