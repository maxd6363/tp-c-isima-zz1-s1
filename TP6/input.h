#ifndef INPUT_H
#define INPUT_H


#include "block.h"
#include "grid.h"
#include "setting.h"


Block input_getUserColor(int numberColors);
Block input_getUserColorSmart(Grid gird, int round, Settings setting);


int keyDetec(void);
void input_clear(void);

void input_hideCursor();
void input_showCursor();
void input_setColorText(int index);


#endif