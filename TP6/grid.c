#include "grid.h"

#include <stdlib.h>
#include <stdio.h>
#include "error.h"
#include "block.h"
#include "bool.h"


#define NB_COLOR 6


Grid grid_init(int size){
	Grid grid;
	grid.size=size;
	grid.array=(Block**)malloc(size * sizeof(Block*));
	if(grid.array == NULL){
		error(__FILE__, __LINE__, "malloc");
		return grid;
	}
	for(int i=0;i<size;i++){		
		grid.array[i]=(Block*)malloc(size * sizeof(Block));
		if(grid.array[i] == NULL){
			error(__FILE__, __LINE__, "malloc");
		}	
	}
	return grid;
}


void grid_free(Grid *grid){
	if(grid == NULL){
		error(__FILE__,__LINE__,"Grid is NULL");
		return;
	}
	if(grid->array != NULL) {
		for(int i=0;i<grid->size;i++){
			if(grid->array[i] != NULL){
				free(grid->array[i]);
			}
		}
		free(grid->array);
		grid->array = NULL;
	}
}


void grid_print(Grid grid){
	for(int i=0;i<grid.size;i++){
		for(int j=0;j<grid.size;j++){
			block_print(grid.array[i][j]);
		}	
		printf("\n");
	}
	printf("\n");
}


void grid_rand(Grid grid, int maxRand){
	if(grid.array != NULL){
		for(int i=0;i<grid.size;i++){
			for(int j=0;j<grid.size;j++){
				grid.array[i][j] = rand()%maxRand;
			}	
		}
	}
}




void grid_spreadColor(Grid grid, Block color){
	if(grid.array != NULL){
		Block initColor = grid.array[0][0];
		internalSpreedColor(grid,initColor,color,0,0);	
	}
}


void internalSpreedColor(Grid grid, Block initColor, Block currentColor, int i, int j){
	if(grid.array == NULL || i<0 || j<0 || i>=grid.size || j>=grid.size || grid.array[i][j] != initColor || grid.array[i][j] == currentColor){
		return;
	}
	grid.array[i][j]=currentColor;
	internalSpreedColor(grid,initColor,currentColor,i+1,j);
	internalSpreedColor(grid,initColor,currentColor,i-1,j);
	internalSpreedColor(grid,initColor,currentColor,i,j-1);
	internalSpreedColor(grid,initColor,currentColor,i,j+1);
}


bool grid_isUniform(Grid grid){
	if(grid.array != NULL){
		Block b = grid.array[0][0];
		for(int i=0;i<grid.size;i++){
			for(int j=0;j<grid.size;j++){
				if(b != grid.array[i][j]){
					return false;
				}
			}
		}
		return true;
	}
	return false;
}

double grid_fillPercentage(Grid grid){
	if(grid.array == NULL){
		return 0;
	}
	int sum = 0;
	for(int i=0;i<grid.size;i++){
		for(int j=0;j<grid.size;j++){
			if(grid.array[0][0] == grid.array[i][j]){
				sum++;
			}
		}
	}
	return ((double)sum) / (grid.size * grid.size);
}

Block grid_getBlock(Grid grid, int x, int y){
	if(grid.array == NULL || x<0 || x>= grid.size || y<0 || y>= grid.size)
		return (Block)-1;
	return grid.array[x][y];

}