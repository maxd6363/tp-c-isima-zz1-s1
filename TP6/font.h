#ifndef FONT_H
#define FONT_H 

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>


TTF_Font * font_init(const char *font, int size);

void font_generate(TTF_Font *font, char * text, SDL_Renderer *renderer, SDL_Texture ** OUT_fontTexture, SDL_Rect *OUT_fontRect, SDL_Surface ** OUT_fontSurface, int x, int y);

void font_quit(TTF_Font *font, SDL_Texture * fontTexture, SDL_Surface * fontSurface);

void font_print(SDL_Texture *fontTexture, SDL_Renderer *renderer, SDL_Rect fontRect);

#endif //FONT_H