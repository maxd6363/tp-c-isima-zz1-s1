#include "block.h"

#include <stdio.h>
#include "input.h"



void block_print(Block b){
	input_setColorText(b);
	printf("██");
	input_setColorText(-1);
}


Color block_getColor(Block b){
	Color c = {.r =0, .g=0, .b=0};
	switch(b){
		case 0:
		c.r=255;
		break;
		case 1:
		c.g=255;
		break;
		case 2:
		c.r=255;
		c.g=255;
		break;
		case 3:
		c.b=255;
		break;
		case 4:
		c.r=255;
		c.b=255;
		break;
		case 5:
		c.g=255;
		c.b=255;
		break;
		case 6:
		c.r=255;
		c.g=255;
		c.b=255;

		break;
	}
	return c;


}

int block_getWidth(SDL_Window *window, int gridSize){
	int screenWidth, screenHeight;
	SDL_GetWindowSize(window,&screenWidth, &screenHeight);
	return (screenHeight > screenWidth ? screenWidth / gridSize: screenHeight / gridSize);

}