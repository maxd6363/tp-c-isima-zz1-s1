#include "game.h"

#include <stdio.h>


#include "setting.h"
#include "graphic.h"


int main(int argc, char const *argv[]){
	SDL_Window* mainWindow;

	Settings settings = setting_get_default();
	if(argc > 1){
		settings = setting_load(argv[1]);
		printf("┌────────────────────────────────────┐\n");
		printf("│"BOLD"             Settings               "RESET"│\n");
		printf("│ Resolution : %d x %d             │\n",settings.windowWidth, settings.windowHeight);
		printf("│ Number of colors : %d               │\n", settings.numberColors);
		printf("│ Grid size : %2d                     │\n",settings.gridSize);
		printf("└────────────────────────────────────┘\n");
	}

	graphic_init();
	mainWindow = graphic_newWindow("FloodIt", settings.windowWidth, settings.windowHeight);
	graphic_newRenderer(mainWindow);
	game_start(settings, mainWindow);
	graphic_freeWindow(mainWindow);
	graphic_exit();
	
	return 0;
}
