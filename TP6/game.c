#include "game.h"

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "input.h"
#include "grid.h"
#include "block.h"
#include "setting.h"
#include "bool.h"
#include "graphic.h"
#include "font.h"
#include "tools.h"

#define MS_FRAME 16


void game_start(Settings settings, SDL_Window *window){
	srand(time(NULL));
	bool playerWon = false;
	bool running = true;
	int round = 0;
	char text[100] = "";
	long lastClicked = 0;
	long timeOutBeforeEnd = 0;


	Grid g;
	Block choice = (Block)-1;

	SDL_Event event;
	SDL_Texture* fontTexture = NULL;
	SDL_Surface* fontSurface = NULL;
	SDL_Rect fontRect;
	SDL_Renderer* renderer = SDL_GetRenderer(window);
	TTF_Font * font;

	g = grid_init(settings.gridSize);
	grid_rand(g, settings.numberColors);
	font = font_init("Font.ttf", 40);


	while (running) {
		game_printGame(g, window);
		font_generate(font, text, renderer, &fontTexture, &fontRect, &fontSurface, block_getWidth(window, g.size) * (g.size + 1), 0);
		font_print(fontTexture, renderer, fontRect);
		game_scoreTextProcess(text, round, settings.maxRound);

		while (SDL_PollEvent(&event)){
			choice = (Block)-1;
			switch(event.type){
				case SDL_WINDOWEVENT:
				switch (event.window.event){
					case SDL_WINDOWEVENT_CLOSE:
					running=false;
					break;
					case SDL_WINDOWEVENT_SIZE_CHANGED:
					game_printGame(g, window);
					font_generate(font, text, renderer, &fontTexture, &fontRect, &fontSurface, block_getWidth(window, g.size) * (g.size + 1), 0);
					font_print(fontTexture, renderer, fontRect);
					break;
				}
				break;
				case SDL_MOUSEBUTTONDOWN:
				choice = grid_getBlock(g,event.button.y / block_getWidth(window, g.size),event.button.x / block_getWidth(window, g.size));
				lastClicked = tools_getCurrentMS();
				break;
				case SDL_QUIT :
				running = false;
				break;
			}
		}	
		SDL_RenderPresent(renderer);
		SDL_Delay(MS_FRAME);

		if(choice != (Block)-1){
			if(tools_getCurrentMS() - lastClicked - MS_FRAME < MS_FRAME){
				round++;
				grid_spreadColor(g, choice);
			}
			if(grid_isUniform(g)){
				playerWon = true;
			}
		}

		if(playerWon || round >= settings.maxRound){
			timeOutBeforeEnd++;
			if(timeOutBeforeEnd == 40){
				break;
			}
		}
	}

	game_end(g, playerWon, window, font);

	grid_free(&g);
	font_quit(font, fontTexture, fontSurface);
}



void game_end(Grid grid, bool won, SDL_Window *window, TTF_Font *font){
	char text[200] = "";
	SDL_Texture* fontTexture = NULL;
	SDL_Surface* fontSurface = NULL;
	SDL_Rect fontRect;
	SDL_Renderer* renderer = SDL_GetRenderer(window);
	font = font_init("Font.ttf", 30);
	sprintf(text, "%s : %02.2lf%% of the grid", won ? "You won !" : "You lose :(", grid_fillPercentage(grid) * 100);
	graphic_clearBackground(renderer);

	font_generate(font, text, renderer, &fontTexture, &fontRect, &fontSurface, 10, 10);
	font_print(fontTexture,  renderer, fontRect);
	SDL_RenderPresent(renderer);
	SDL_Delay(3000);
}


void game_printGame(Grid grid, SDL_Window *window){
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	graphic_clearBackground(renderer);
	int blockWidth = block_getWidth(window, grid.size);
	if(grid.array != NULL){
		for(int i=0;i<grid.size;i++){
			for(int j=0;j<grid.size;j++){
				graphic_drawBlock(renderer,blockWidth, j, i, grid.array[i][j]);
			}
		}
	}
}



void game_scoreTextProcess(char * text, int currentRound, int maxRound){
	sprintf(text, "%02d / %02d", currentRound, maxRound);
}


