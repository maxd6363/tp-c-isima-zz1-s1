#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#define NB_GUICHET 10

int * guichets[NB_GUICHET];

int main(){

	int i, guichetToOpen;
	srand(time(NULL));
	guichetToOpen = rand()%NB_GUICHET;
	
	for(i=0;i<NB_GUICHET;i++){
		guichets[i] = (int*)malloc(sizeof(int));
		if(guichets[i] == NULL){
			fprintf(stderr, "ERROR MALLOC\n" );
			return -1;
		}
		*guichets[i] = 0;
	}


	*guichets[guichetToOpen] = 1;


	for(i=0;i<NB_GUICHET;i++){
		printf("%d : %s\n",i, *guichets[i]==0?"FERMÉ":"OUVERT");
	}


	for(i=0;i<NB_GUICHET;i++){
		free(guichets[i]);
	}


	return 0;
}