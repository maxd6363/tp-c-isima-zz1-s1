#ifndef BINARY
#define BINARY

typedef struct{
	
	char author[100];
	char name[100];
	int year;

}Language;


Language* binary_load(int * number, char* filename);
void binary_save(Language* languages, int number, char* filename);

void binary_print(Language* languages, int number);


#endif //BINARY