#include "binary.h"

#include <stdlib.h>
#include <stdio.h>

#define MAX_ARRAY_SIZE 100


Language* binary_load(int * number, char* filename){
	if(filename == NULL){
		fprintf(stderr, "Error in loading file\n");
		return NULL;
	}
	Language * array =(Language*)calloc(MAX_ARRAY_SIZE, sizeof(Language));
	Language tmp;
	int index = 0;
	if(array == NULL){
		return NULL;
	}

	FILE * file = fopen(filename, "rb");
	if(file == NULL){
		fprintf(stderr, "Can't open file %s\n", filename);
		return NULL;
	}
	fread(&tmp,sizeof(Language),1, file);
	while(!feof(file)){
		array[index]=tmp;
		fread(&tmp,sizeof(Language),1, file);
		index++;
	}
	*number=index;
	fclose(file);
	return array;

}



void binary_save(Language* languages, int number, char* filename){
	if(languages == NULL || filename == NULL || number <= 0){
		fprintf(stderr, "Error in saving file\n");
		return;
	}

	FILE * file = fopen(filename, "wb");
	if(file == NULL){
		fprintf(stderr, "Can't open file %s\n", filename);
		return;
	}
	fwrite(languages,sizeof(Language),number,file);

	fclose(file);
}


void binary_print(Language* languages, int number){
	if(languages == NULL || number <= 0){
		return;
	}

	for(int i=0;i<number;i++){
		printf("%-30s%-15s\t%d\n",languages[i].author,languages[i].name,languages[i].year);
	}
	printf("\n\n");
}