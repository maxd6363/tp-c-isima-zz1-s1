#include "binary.h"

#include <stdio.h>
#include <stdlib.h>


int main(void){
	
	Language lang[15] = {
		{"Backus John","FORTRAN",1957},
		{"Goldfarb Charles","GML",1969},
		{"Wirth Niklaus","Pascal",1970},
		{"Ritchie Dennis","C",1972},
		{"Kernighan Brian","C",1972},
		{"Sussman Gerald Jay","Scheme",1975},
		{"Steele Guy","Scheme",1975},
		{"Naughton Patrick","Java",1983},
		{"Cox Brad","Objective C",1983},
		{"Berners Lee Tim","WWW",1990},
		{"Gosling James","Java",1993},
		{"Lerdorf Rasmus","PHP",1994},
		{"Heich Brendan","Javscript",1995},
		{"Odersky Martin","Scala",2003},
		{"Lattner Chris","Swift",2014}
	};

	printf("BEFORE : \n");
	binary_print(lang, 15);
	printf("\n\n\n");

	binary_save(lang,15,"save001.dat");
	int number;
	Language *loaded = binary_load(&number,"save001.dat");

	printf("AFTER : \n");
	binary_print(loaded, number);
	printf("\n\n\n");


	free(loaded);

	return 0;
}