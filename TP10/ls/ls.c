#include "ls.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/types.h>
#include <dirent.h>


#include "color.h"

void ls_print_directory_content(char * directory){
	if(directory == NULL){
		return;
	}

	struct dirent *dirStruct;
	DIR *dir;
	dir = opendir(directory);
	if(dir == NULL){
		return;
	}
	printf(RED "%s\n" RESET, directory);
	while((dirStruct = readdir(dir))){
		printf("\t%c  %s\n",ls_dir_type(dirStruct->d_type), dirStruct->d_name);
	}
	printf("\n");
	closedir(dir);
}





void ls_print(int count, ...){
	va_list parameters;
	va_start(parameters, count);
	char *param;

	for(;count > 0;count--){
		param = (char*)va_arg(parameters, char*);
		ls_print_directory_content(param);
	}


	va_end(parameters);
}



char ls_dir_type(int type){
	switch(type){
		case DT_REG:
		return '-';
		case DT_DIR:
		return 'd';
		case DT_CHR:
		return 'c';
		case DT_BLK:
		return 'b';
		case DT_SOCK:
		return 's';
		case DT_FIFO:
		return 'p';
		case DT_LNK:
		return 'l';
		default :
		return ' ';
	}
}
