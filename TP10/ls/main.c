#include <stdio.h>
#include <stdlib.h>

#include "ls.h"


int main(int argc, char const *argv[]){
	
	int count = argc - 1;

	switch(count){
		case 0:
		ls_print(1,".");
		break;
		case 1:
		ls_print(count,argv[1]);
		break;
		case 2:
		ls_print(count,argv[1], argv[2]);
		break;
		case 3:
		ls_print(count,argv[1], argv[2], argv[3]);
		break;
		case 4:
		ls_print(count,argv[1], argv[2], argv[3], argv[4]);
		break;
		case 5:
		ls_print(count,argv[1], argv[2], argv[3], argv[4], argv[5]);
		break;
		case 6:
		ls_print(count,argv[1], argv[2], argv[3], argv[4], argv[5], argv[6]);
		break;
	}


	return 0;
}

