#ifndef COMMON
#define COMMON

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "histo.h"



#define DEBUG

#ifdef DEBUG
    #define LOG(A) printf A
#else
    #define LOG(A) 
#endif



#define ERROR_OK          0
#define ERROR_LIST_ALLOC  1
#define ERROR_FILE        1


typedef struct gdata_s {
    SDL_Window   * window;
    SDL_Renderer * renderer;
    TTF_Font     * font;       
    int            width;
    int            height;
} gdata_t;


void displayString(gdata_t g, char * chaine, int x, int y);
void displayGraphicalHisto(gdata_t g, histogram_t h);                          
void displayGraph(histogram_t h);
void displayText(histogram_t h);


#endif