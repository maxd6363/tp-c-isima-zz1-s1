#ifndef HISTO
#define HISTO

#define HISTOSIZE 21

#include "list.h"


typedef int histogram_t[HISTOSIZE];

void computeHisto(histogram_t h, list_t l);
void displayHisto(histogram_t h);
int maxHisto(histogram_t h);
float meanHisto(histogram_t h);
int countHisto(histogram_t h);



#endif